var studentsAndPoints = [
  'Алексей Петров',0,
  'Ирина Овчинникова',60,
  'Глеб Стукалов',30,
  'Антон Павлович',30,
  'Виктория Заровская',30,
  'Алексей Левенец',70,
  'Тимур Вамуш',30,
  'Евгений Прочан',60,
  'Александр Маслов',0];

// 1. Создание массива students

var students = [];

studentsAndPoints.forEach(function(item,index)
{
  if (index%2==0)
  {
    students.push({
      name: item,
      point: studentsAndPoints[index+1],
      show: function()
      {
        console.log('Студент %s набрал %d баллов',this.name,this.point);
      }
    });
  }
});

// 2. Добавление студентов

students.push({
  name: 'Николай Фролов',
  point: 0,
  show: students[0].show
},{
  name: 'Олег Боровой',
  point: 0,
  show: students[0].show
});

// 3. Увеличение баллов

students.forEach(function(item)
{
  switch (item.name)
  {
    case 'Ирина Овчинникова':
    case 'Александр Маслов':
      item.point += 30;
      break;
    case 'Николай Фролов':
      item.point += 10;
      break;
  }
});

// 4. Вывод списка студентов, набравших 30 и более баллов

students.forEach(function(item)
{
  if (item.point>=30)
  {
    item.show();
  }
});

// 5. Добавление поля worksAmount

students.forEach(function(item)
{
  item.worksAmount = Math.floor(item.point/10); // кол-во работ должно быть целым; floor на всякий случай
});

// 6. Дополнительное задание: добавление метода findByName

students.findByName = function(name)
{
  var result;
  students.forEach(function(item)
  {
    if (item.name==name) result = item;
  });
  return result;
};